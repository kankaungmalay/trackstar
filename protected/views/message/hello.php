<?php
/* @var $this MessageController */

$this->breadcrumbs=array(
	'Message'=>array('/message'),
	'Hello',
);
?>
	<h1>Hello World!</h1>
	<h3><?php echo $this->time; ?></h3>
	<a href="/yii_app/index.php?r=message/goodbye">Goodbye!</a>
	<p><?php echo CHtml::link('Goodbye' , array('message/goodbye')); ?></p>
